module.exports = {
  "extends": [
    "airbnb-base",
    "plugin:prettier/recommended"
  ],
  "plugins": [
    "prettier"
  ],
  "rules": {
    "prettier/prettier": "error",
    "linebreak-style": [
      "error",
      "unix"
    ],
    "global-require": 0,
    "no-multi-spaces": 2,
    "prefer-destructuring": 1,
    "prettier/prettier": ["error", {
      tabWidth: 4,
      singleQuote: true,
      trailingComma: "es5",
      printWidth: 120,
      arrowParens: "always",
    }],
  }
};
