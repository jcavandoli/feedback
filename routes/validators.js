const Joi = require('joi');

const { IDENTIFIER, MESSAGE, QUERY, RATING, STATUS_CODE } = require('../constants');

const rating = Joi.number()
    .integer()
    .positive()
    .precision(0)
    .min(RATING.MIN)
    .max(RATING.MAX);

const message = Joi.string()
    .min(MESSAGE.MIN)
    .max(MESSAGE.MAX);

const limit = Joi.number()
    .integer()
    .positive()
    .precision(0)
    .min(1)
    .max(QUERY.MAX_LIMIT);

const sortOrder = Joi.string().valid('desc', 'asc');

const identifier = Joi.string().max(IDENTIFIER.MAX);

const headers = Joi.object({
    'ubi-userid': identifier.required(),
}).options({
    // By default Joi reject custom headers
    allowUnknown: true,
});

const failAction = (request, reply, err) => reply.response({ message: err.message }).code(STATUS_CODE.BAD_REQUEST);

const addFeedbackValidators = {
    failAction,
    headers,
    // Uri parameter. `/feedback/sessionId/{sessionId}`
    params: {
        sessionId: identifier.required(),
    },
    payload: {
        message: message.required(),
        rating: rating.required(),
    },
};

const getFeedbacksValidators = {
    failAction,
    // Query parameters. Eg. `?rating=1&limit=500&sortOrder=desc`
    query: {
        rating,
        limit,
        sortOrder,
    },
};

module.exports = {
    addFeedbackValidators,
    getFeedbacksValidators,
};
