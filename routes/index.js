const { addFeedbackHandler, getFeedbacksHandler, healthCheck } = require('./handlers');
const { addFeedbackValidators, getFeedbacksValidators } = require('./validators');

module.exports = (server) => {
    server.route([
        {
            method: 'GET',
            path: '/health',
            handler: healthCheck,
        },
        {
            method: 'POST',
            path: '/feedback/session/{sessionId}',
            handler: addFeedbackHandler,
            config: {
                validate: addFeedbackValidators,
            },
        },
        {
            method: 'GET',
            path: '/feedbacks',
            handler: getFeedbacksHandler,
            config: {
                validate: getFeedbacksValidators,
            },
        },
    ]);
};
