const mongoose = require('mongoose');
const { Feedback } = require('../models');
const { QUERY, STATUS_CODE } = require('../constants');

const healthCheck = (request, reply) => {
    const mongoStatus = mongoose.connection && mongoose.connection.readyState;
    const statusCode = mongoStatus === 1 ? STATUS_CODE.SUCCESS : STATUS_CODE.SERVICE_UNAVAILABLE;
    return reply.response({ mongoStatus }).code(statusCode);
};

const getFeedbacksHandler = (request, reply) => {
    const criteria = {};
    if (request.query.rating) {
        criteria.rating = request.query.rating;
    }
    Feedback.find(criteria)
        .limit(request.query.limit || QUERY.DEFAULT_LIMIT)
        .sort({ createdAt: request.query.sortOrder || QUERY.DEFAULT_SORT_ORDER })
        .exec((err, docs) => {
            if (err) {
                return reply.response({ message: err.message }).code(STATUS_CODE.ERROR_INTERNAL);
            }
            return reply.response(docs).code(STATUS_CODE.SUCCESS);
        });
};

const addFeedbackHandler = (request, reply) => {
    const payload = {
        ...request.payload,
        sessionId: request.params.sessionId,
        playerId: request.headers['ubi-userid'],
    };
    Feedback.create(payload, (err) => {
        if (err) {
            return reply.response({ message: err.message }).code(err.code || STATUS_CODE.ERROR_INTERNAL);
        }
        return reply.response({}).code(STATUS_CODE.CREATED);
    });
};

module.exports = {
    addFeedbackHandler,
    getFeedbacksHandler,
    healthCheck,
};
