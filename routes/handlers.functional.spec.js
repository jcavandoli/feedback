const { IDENTIFIER, RATING, MESSAGE, QUERY, STATUS_CODE, TESTING } = require('../constants');
const { Feedback } = require('../models');
const crypto = require('crypto');
const server = require('..');

const noop = () => {};
const generateRandomId = () => crypto.randomBytes(16).toString('hex');
const generateRandomMessage = () => crypto.randomBytes(128).toString('hex');
const generateRandomRating = () => Math.floor(Math.random() * (RATING.MAX - RATING.MIN + 1)) + RATING.MIN;

/**
 * Generates a feedback entry with randomized playerId and message.
 * It's possible to pass a rating to enforce a specific value.
 */
const generateRamdomEntry = (sessionId, rating = generateRandomRating()) => ({
    sessionId,
    rating,
    playerId: generateRandomId(),
    message: generateRandomMessage(),
});

/**
 * Generates a list of feedback entries with randomized properties.
 * Divides the users between given number of game sessions.
 */
const generateRandomEntries = (enforcedRating) => {
    let sessionId = generateRandomId();
    const playersBySession = Math.floor(TESTING.NB_PLAYER / TESTING.NB_SESSION);
    const entries = [];
    for (let i = 1; i < TESTING.NB_PLAYER; i += 1) {
        if (i % playersBySession === 0) {
            sessionId = generateRandomId();
        }
        entries.push(generateRamdomEntry(sessionId, enforcedRating));
    }
    return entries;
};

const sessionId = generateRandomId();
const playerId = generateRandomId();

const dropCollection = (done) => Feedback.collection.drop(done);
const insertTestData = (done) => Feedback.insertMany(generateRandomEntries(), done);

describe('API - functional tests', () => {
    beforeAll(insertTestData);

    afterAll(dropCollection);

    describe('GET /health', () => {
        it(`should respond a ${STATUS_CODE.SUCCESS} status code`, () => {
            server
                .inject({
                    method: 'GET',
                    url: '/health',
                })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                    expect(response.result).toEqual({ mongoStatus: 1 });
                });
        });
    });

    describe('POST /feedback/session/{sessionId}', () => {
        const request = {
            method: 'POST',
            url: `/feedback/session/${sessionId}`,
            headers: {
                'Ubi-UserId': playerId,
            },
            payload: {
                message: 'OMG I had so much fun!',
                rating: 5,
            },
        };

        it('should add a new feedback', () => {
            server.inject(request).then((response) => {
                expect(response.statusCode).toEqual(STATUS_CODE.CREATED);
            });
        });

        it(`should return a ${STATUS_CODE.CONFLICT} status code when adding feedback with an existing sessionId / playerId combination`, () => {
            server.inject(request).then((response) => {
                expect(response.statusCode).toEqual(STATUS_CODE.CONFLICT);
            });
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the message is missing in the payload`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, message: '' } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the message length is under ${MESSAGE.MIN} characters`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, message: 'z'.repeat(MESSAGE.MIN - 1) } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the message length is higher than ${MESSAGE.MAX} characters`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, message: 'z'.repeat(MESSAGE.MAX + 1) } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the rating is under ${RATING.MIN}`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, rating: RATING.MIN - 1 } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the rating is higher than ${RATING.MAX}`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, rating: RATING.MAX + 1 } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the rating is missing`, () => {
            server
                .inject({ ...request, payload: { ...request.payload, rating: null } })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the user identifier is missing in headers`, () => {
            server
                .inject({ ...request, headers: '' })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code if the user identifier is higher than ${IDENTIFIER.MAX}`, () => {
            server
                .inject({ ...request, headers: {'Ubi-UsedId': 'z'.repeat(IDENTIFIER.MAX + 1)} })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                })
                .catch(noop);
        });
    });

    describe('GET /feedbacks', () => {
        const options = {
            method: 'GET',
            url: '/feedbacks',
        };
        it('should get the 15 last feedbacks (default)', () => {
            server.inject(options).then((response) => {
                expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                expect(response.result).toBeDefined();
                expect(response.result.length).toEqual(15);
            });
        });

        it('should get the 75 last feedbacks', () => {
            server
                .inject({
                    ...options,
                    url: '/feedbacks?limit=75',
                })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                    expect(response.result).toBeDefined();
                    expect(response.result.length).toEqual(75);
                });
        });

        it('should get the 5 last feedbacks', () => {
            server
                .inject({
                    ...options,
                    url: '/feedbacks?limit=5',
                })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                    expect(response.result).toBeDefined();
                    expect(response.result.length).toEqual(5);
                });
        });

        it('should get the 10 last feedbacks rated 4', (done) => {
            const enforcedRating = 4;
            Feedback.insertMany(generateRandomEntries(enforcedRating), (err) => {
                if (err) {
                    return done(err);
                }
                return server
                    .inject({
                        ...options,
                        url: `/feedbacks?limit=10&rating=4`,
                    })
                    .then((response) => {
                        expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                        expect(response.result).toBeDefined();
                        expect(response.result.length).toEqual(10);
                        response.result.forEach((e) => expect(e.rating).toEqual(4));
                        done();
                    });
            });
        });

        it('should get the 15 last feedbacks sorted `desc` (default)', () => {
            server.inject(options).then((response) => {
                expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                expect(response.result).toBeDefined();
                expect(response.result.length).toEqual(15);

                // Check if previous date is higher
                response.result.forEach((doc, index) => {
                    if (doc[index - 1]) {
                        expect(doc.created > doc[index - 1].created);
                    }
                });
            });
        });

        it('should get the 15 last feedbacks sorted `asc`', () => {
            server.inject(options).then((response) => {
                expect(response.statusCode).toEqual(STATUS_CODE.SUCCESS);
                expect(response.result).toBeDefined();
                expect(response.result.length).toEqual(15);

                // Check if previous date is lower
                response.result.forEach((doc, index) => {
                    if (doc[index - 1]) {
                        expect(doc.created < doc[index - 1].created);
                    }
                });
            });
        });

        it(`should return a ${STATUS_CODE.BAD_REQUEST} status code is the limit is higher than ${QUERY.MAX_LIMIT}`, () => {
            server
                .inject({
                    ...options,
                    url: `/feedbacks?limit=${QUERY.MAX_LIMIT + 1}`,
                })
                .then((response) => {
                    expect(response.statusCode).toEqual(STATUS_CODE.BAD_REQUEST);
                });
        });
    });
});
