# Feedback Microservice

The Feedback Microservice allows players to leave a feedback about their game sessions.

The live OPs team can then fetch the last feedbacks left by players.


Here are the API specifications: https://app.swaggerhub.com/apis/jca/feedback/1.0.0

---

## How to run the service


### Clone this repository using git by running
`git clone https://gitlab.com/jcavandoli/feedback.git`

You can choose between using Docker or installing Nodejs and MongoDB dependencies locally.

### Using Docker

#### Prerequisites
- Docker installed and running (v18.x).
- Docker Compose installed (v1.22.x).

#### Run
`docker-compose up`

That's it, the service is now running on:  `http://localhost:3000/`

### On your local machine

#### Prerequisites
- Nodejs installed (v8.x Carbon).
- Npm installed (v5.x).
- MongoDB installed (v4.x) and running on default port `27017`.

#### Install dependencies
`npm install`

#### Run service
`npm start`

That's it, the service is now running on:  `http://localhost:3000/`

## Tests
You can run the unit and functional tests locally just by running `npm test`.

# Request examples

### Post a new feedback
```
curl -i -X POST \
  http://localhost:3000/feedback/session/2738b027904fa91da4028fa1b4ab3107 \
  -H 'Ubi-UserId: f3cfe459344161917c7eadd19e043e50' \
  -F rating=5 \
  -F message='I really love this game <3'
  ```

### Get the feedbacks (last 15 by default)
```
curl -i -X GET http:/localhost:3000/feedbacks
```

### Get the 10 last feedbacks rated 5
```
curl -i -X GET http:/localhost:3000/feedbacks?rating=5&limit=10
```

---

Also, you can reach an Amazon AWS EC2 hosted instance of the microservice running here:

`ec2-18-220-91-172.us-east-2.compute.amazonaws.com:3000` 
