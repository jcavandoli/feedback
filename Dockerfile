FROM node:carbon
RUN mkdir -p /home/app
WORKDIR /home/app
COPY package.json /home/app
COPY package-lock.json /home/app
ENV NODE_ENV=production
RUN npm install
COPY . /home/app
EXPOSE 3000
CMD ["npm", "start"]
