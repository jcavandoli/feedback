module.exports = {
    QUERY: {
        DEFAULT_LIMIT: 15,
        DEFAULT_SORT_ORDER: 'desc',
        MAX_LIMIT: 1000,
    },
    RATING: {
        MIN: 1,
        MAX: 5,
    },
    MESSAGE: {
        MIN: 1,
        MAX: 1000,
    },
    IDENTIFIER: {
        MAX: 128,
    },
    STATUS_CODE: {
        SUCCESS: 200,
        CREATED: 201,
        BAD_REQUEST: 400,
        CONFLICT: 409,
        ERROR_INTERNAL: 500,
        SERVICE_UNAVAILABLE: 503,
    },
    TESTING: {
        NB_PLAYER: 100,
        NB_SESSION: 5,
    },
};
