const Hapi = require('hapi');
const mongoose = require('mongoose');

const server = new Hapi.Server();

const connectDatabase = () => {
    const database = process.env.MONGO_DATABASE || 'feedback';
    const host = process.env.NODE_ENV === 'production' ? 'database' : 'localhost';
    mongoose.connect(
        `mongodb://${host}/${database}`,
        { useNewUrlParser: true }
    );
    mongoose.Promise = global.Promise;

    // Register models
    require('./models');
};

const onError = (err) => {
    console.log(err);
    process.exit(1);
};

// Global database error handling
mongoose.connection.on('error', onError);

// Global process error handling
process.on('unhandledRejection', onError);

server.connection({ port: 3000 });
server.start((err) => {
    if (err) {
        return onError(err);
    }

    // Register routes
    require('./routes')(server);

    console.log(`Server running at: ${server.info.uri}`);
    return connectDatabase();
});

module.exports = server;
