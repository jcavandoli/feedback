const { IDENTIFIER, RATING, MESSAGE, QUERY, STATUS_CODE, TESTING } = require('../constants');
const Feedback = require('./feedback');

describe('Feedback model - unit tests', () => {
    const payload = {
        message: 'WOOT! this game rocks!',
        rating: 5,
        sessionId: '14188c0711aee5149052956136e1c56d',
        playerId: '4ce8443f29be51aa92b3d0409e7e815b',
    };

    it('it should pass the validation', (done) => {
        const feedback = new Feedback(payload);
        feedback.validate((err) => {
            expect(err).toBeNull();
            done();
        });
    });

    it(`it should not be valid if the message is under ${MESSAGE.MIN} characters`, (done) => {
        const feedback = new Feedback({ ...payload, message: 'z'.repeat(MESSAGE.MIN - 1)});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(err.errors.message).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the message is under ${MESSAGE.MIN} characters`, (done) => {
        const feedback = new Feedback({ ...payload, message: 'z'.repeat(MESSAGE.MIN - 1)});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(err.errors.message).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the message is higher than ${MESSAGE.MAX} characters`, (done) => {
        const feedback = new Feedback({ ...payload, message: 'z'.repeat(MESSAGE.MAX + 1)});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.message).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the rating is under ${RATING.MIN} characters`, (done) => {
        const feedback = new Feedback({ ...payload, rating: RATING.MIN - 1});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.rating).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the rating is under ${RATING.MIN} characters`, (done) => {
        const feedback = new Feedback({ ...payload, rating: RATING.MIN - 1});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.rating).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the rating is higher than ${RATING.MAX} characters`, (done) => {
        const feedback = new Feedback({ ...payload, rating: RATING.MAX + 1});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.rating).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the sessionId is higher than ${IDENTIFIER.MAX} characters`, (done) => {
        const feedback = new Feedback({ ...payload, sessionId: 'z'.repeat(IDENTIFIER.MAX + 1)});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.sessionId).toBeDefined();
            done();
        });
    });

    it(`it should not be valid if the playerId is higher than ${IDENTIFIER.MAX} characters`, (done) => {
        const feedback = new Feedback({ ...payload, playerId: 'z'.repeat(IDENTIFIER.MAX + 1)});
        feedback.validate((err) => {
            expect(err).not.toBeNull();
            expect(err.errors).toBeDefined();
            expect(Object.keys(err.errors).length).toBe(1);
            expect(err.errors.playerId).toBeDefined();
            done();
        });
    });
});