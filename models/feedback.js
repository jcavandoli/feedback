const mongoose = require('mongoose');
const { IDENTIFIER, MESSAGE, RATING, STATUS_CODE } = require('../constants');

const feedbackSchema = new mongoose.Schema({
    message: {
        type: String,
        minlength: MESSAGE.MIN,
        maxlength: MESSAGE.MAX,
        required: true,
    },
    rating: {
        type: Number,
        min: RATING.MIN,
        max: RATING.MAX,
        // Indexing here so the queries filtered by rating run faster
        index: true,
        required: true,
    },
    sessionId: {
        type: String,
        maxlength: IDENTIFIER.MAX,
        required: true,
    },
    playerId: {
        type: String,
        maxlength: IDENTIFIER.MAX,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

feedbackSchema.pre('save', function(next) {
    // Ensure there's no already any entry for the player / session combination
    const { sessionId, playerId } = this;
    this.model('Feedback').countDocuments({ sessionId, playerId }, (err, count) => {
        if (count) {
            const error = new Error(`There is already a feedback entry by user ${playerId} for session ${sessionId}`);
            error.code = STATUS_CODE.CONFLICT;
            return next(error);
        }
        return next(err);
    });
});

// Ensure we don't insert multiple feedbacks with the same user and session by using a compound index
// ref: https://docs.mongodb.com/manual/core/write-operations-atomicity/#concurrency-control
feedbackSchema.index(
    {
        playerId: 1,
        sessionId: 1,
    },
    { unique: true }
);

module.exports = mongoose.model('Feedback', feedbackSchema);
